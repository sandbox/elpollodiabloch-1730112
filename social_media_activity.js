(function($, Drupal) {
  var smaIntervalId;
  var smaChart;
  var smaArrayOfData;
  
  Drupal.behaviors.socialMediaActivityModule = {
    attach: function (context, settings) {
      //TODO: there must be a better solution to prevent the script from running on all pages!
      if(Drupal.settings.social_media_activity) 
        smaExecute();
    }
  }
  
  var smaExecute = function()
  {    
    smaUpdate(true);
    smaIntervalId = setInterval(function(){
      smaUpdate(false);
    }, Drupal.settings.social_media_activity.interval);
  }
  
  var smaUpdate = function(animate)
  {
    $.getJSON(Drupal.settings.social_media_activity.update_url, function(data) {
      //console.debug(data);
      smaArrayOfData = new Array([[data.facebook, data.twitter, data.remaining], '']);
      $('#sma-chart').empty();
      $('#sma-chart').jqBarGraph({
        data: smaArrayOfData,
        colors: ['#3b5998','#9AE4E8', '#CCCCCC'],
        width: 400,
        height: 300,
        animate: animate,
        legend: true,
        legendWidth: 320,
        legends: ['Facebook: ' + data.facebook, 'Twitter: ' + data.twitter, 'Remaining: ' + data.remaining],
        showValues: false,
        title: false
      });
      $('#target_content').html(data.content);
    });
  };
})(jQuery, Drupal);
